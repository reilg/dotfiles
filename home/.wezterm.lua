-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- Custom config starts here

-- Startup
config.default_prog = { "tmux", "-2" }

-- Colorscheme
config.color_scheme = "nord"
-- config.color_scheme = "Nord (base16)"
-- config.color_scheme = "Nord (Gogh)"

-- Window decorations
config.window_background_opacity = 0.9
config.window_decorations = "TITLE | RESIZE"

--  Window borders
config.window_frame = {
  border_left_width = "0.1 cell",
  border_left_color = "#000",
}

-- Window Intiail Size
config.initial_rows = 40
config.initial_cols = 120

config.font = wezterm.font_with_fallback({
  "Hack Nerd Font",
  "JetBrains Mono",
  "Noto Color Emoji",
  { family = "Symbols Nerd Font", scale = 1 },
})
config.font_size = 9

config.enable_tab_bar = false
config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0,
}

wezterm.on("format-window-title", function(tab, pane, tabs, panes, config)
  local icons = wezterm.nerdfonts

  local title = {
    icons.md_pac_man,
    icons.md_ghost_outline,
    icons.md_ghost_outline,
    "|",
    icons.md_nintendo_switch,
    icons.md_linux,
  }

  return table.concat(title, "   ")
end)

-- Set right status
wezterm.on("update-right-status", function(window)
  local date = wezterm.strftime("%a %b %-d %H:%M ")

  window:set_right_status(wezterm.format({
    { Text = wezterm.nerdfonts.fa_clock_o .. " " .. date },
  }))
end)

-- and finally, return the configuration to wezterm
return config
