# Example script for using stow to track home config files
all:
	stow --verbose --target=$$HOME --restow home

delete:
	stow --verbose --target=$$HOME --delete home
