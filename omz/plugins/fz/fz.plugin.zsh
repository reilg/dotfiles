# Install z
export _Z_CMD="j"
source $ZSH_CUSTOM/plugins/fz/z.sh

# Install fz
export FZ_CMD="j"
export FZ_SUBDIR_CMD="jj"
source $ZSH_CUSTOM/plugins/fz/fz.sh
