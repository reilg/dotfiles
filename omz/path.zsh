# If notes repo is present, add script to path
NOTES_DIR="$HOME/Projects/notes"
if [ -f "$NOTES_DIR/bin/notes" ]; then
  PATH="$PATH:$NOTES_DIR/bin"
fi

# GoLang paths, g command
export GOPATH="$HOME/go"
export GOROOT="$HOME/.go"
export PATH="$GOPATH/bin:$PATH"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

# Add /snap/bin
export PATH="$PATH:/snap/bin"

export N_PREFIX="$HOME/n"; [[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"  # Added by n-install (see http://git.io/n-install-repo).
