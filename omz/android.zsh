# Android Studio related stuff


# if on macos
if [[ $(uname) == "Darwin" ]]; then
  export ANDROID_HOME=$HOME/Library/Android/Sdk
else
  # if on anything else
  export ANDROID_HOME=$HOME/Android/Sdk
fi

export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/platform-tools
