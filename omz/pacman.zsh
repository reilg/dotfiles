# Easy system package maintenance
alias mxpacman='sudo apt'
alias mxupdate='mxpacman update'
alias mxinstall='mxpacman install'
alias mxupgrade='mxpacman upgrade'
alias mxdistupgrade='mxpacman dist-upgrade'
alias mxautoclean='mxpacman autoclean'
alias mxautoremove='mxpacman autoremove'
alias mxmaintenance='mxupdate && mxupgrade -y && mxdistupgrade -y && mxautoclean -y && mxautoremove -y'

# New aliases
alias xpac='sudo apt'
alias xinst='xpac install'
alias xupd='xpac update'
alias xupg='xpac upgrade'
alias xdistupgrade='xpac dist-upgrade'
alias xautoclean='xpac autoclean'
alias xautoremove='xpac autoremove'

# Prefer nala over apt
if [ $(command -v nala) ]
then
  alias mxpacman='sudo nala'
  alias xpac='sudo nala'
  alias maint='xupd && xupg -y && xautoremove -y'
  alias mxmaintenance='maint'
else
  alias maint='xupd && xupg -y && xdistupgrade -y && xautoclean -y && xautoremove -y'
  alias mxmaintenance='maint'
fi

alias shut='mxmaintenance && shutdown now'
