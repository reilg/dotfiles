# Easy larasail implementation
alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'

# Sail - Docker
alias s='sail'
alias sup='sail up'
alias supd='sail up -d'

# Sail Artisan
alias sa='sail artisan'
alias sam='sail artisan migrate'
alias samf='sail artisan migrate:fresh'
alias samfs='sail artisan migrate:fresh --seed'

# Sail Tinker
alias stink='sail tinker'

# Sail - NPM
alias sn='sail npm'
alias sndev='sail npm run dev'

