# mysql aliases
alias mce="mysql_config_editor"

myc() { mysql --login-path=$1 "${@:2}" }

_myc() {
  local profiles
  profiles=($(mysql_config_editor print --all 2>/dev/null | grep -oP '^\[\K[^]]+'))

  # Provide completion suggestions
  compadd -a profiles
}

compdef _myc myc
