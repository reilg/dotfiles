bindkey '^ ' autosuggest-accept
bindkey '^y' autosuggest-accept

# Define a function to show ghost text for aliases
_ghost_alias_expand() {
  local input=${BUFFER%% *} # Get the first word in the buffer

  if alias_expansion=$(alias "$input" 2>/dev/null); then
    alias_expansion="${alias_expansion#*=}" # Strip out alias= part
    alias_expansion="${alias_expansion//\'/}" # Remove single quotes

    faded_color=$'\e[38;5;244m'  # Gray color (adjust the number for different shades)
    reset_color=$'\e[0m'       # Reset to default color

    # zle -M "${faded_color}${alias_expansion}${reset_color}"
    zle -M $alias_expansion
  else
    zle -M ""
  fi
}

# Attach the function to the `zle` widget system
# autoload -Uz add-zle-hook-widget
# add-zle-hook-widget line-pre-redraw _ghost_alias_expand
