# Neovim aliases

# Use kickstart setup
alias vk="NVIM_APPNAME=nvim.ks vi"

# Use folke/LazyVim setup
alias vl="NVIM_APPNAME=nvim.lz vi"

# Use my LazyVim test setup
alias vm="NVIM_APPNAME=nvim.me vi"

# Use neovim-configs/just-lazy setup
alias vj="NVIM_APPNAME=nvim.jz vi"
