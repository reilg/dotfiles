# Set editors to neovim
alias v='vi'
alias vi='nvim'

# Config file helpers
alias zconf="vi ~/.zshrc"
alias tconf="vi ~/.tmux.conf"
alias wconf="vi ~/.wezterm.lua"

if  uname -r | grep -q 'microsoft'
then
  alias wconf="vi /mnt/c/Users/reeei/.wezterm.lua"
fi

# Unset g=git alias
unalias g
alias gbvl="git branch --sort=-committerdate --format=\"%(align:35)%(committerdate:rfc)%(end) %(align:10)%(color:green)%(committerdate:relative)%(color:reset)%(end) %(align:20)%(refname:short)%(end) %(align:50,left)%(color:green)%(contents:subject)%(color:reset)%(end) \""
alias gbsort="git branch --sort=-committerdate"

# Fdfind is now fd
if [ $(command -v fdfind) ]
then
  alias fd='fdfind'
fi
alias fdir='fd -t d'

# Default ripgrep to smart case
alias rg='rg -S'

# Better lists
alias ls='ls -v --color=tty --group-directories-first'
alias lg='la | rg'

# Alias for easy clipping
if [ $(command -v xsel) ]
then
  alias clip='xsel -b'
fi

# Show paths
alias paths="sed 's/:/\n/g' <<< \"$PATH\""

# MyCli aliases
alias my="mycli -u $USER"

# Show DNS
alias dns='(nmcli dev list || nmcli dev show) 2>/dev/null | ag DNS'

# Get my IP
alias xip='curl https://api.ipify.org'

# scrcpy shiz
if [ $(command -v scrcpy) ]
then
	alias rs8='nohup scrcpy -m 1080 --window-height 750 -S &'
	alias rs21='nohup scrcpy -b2M -m800 -S &'
	alias rphone='nohup scrcpy --window-height 900 -S &'
	alias rwphone='nohup scrcpy -b2M --window-height 900 -S &'
  alias phwifi='scrcpy --tcpip'
fi
